public class Task4 {

    public static void main(String[] args) {
        int[] array = new int[8];

        int i;
        for (i = 0; i < 8; ++i) {
            int random = (int) (Math.random() * 11);
            array[i] = random;
        }

        for (i = 0; i < 7; ++i) {
            System.out.print(array[i] + " ");
        }
        System.out.print(array[array.length - 1]);
        System.out.println();


        boolean sorted = true;
        for (i = 0; i < array.length; i++) {
            if (array[i] > array[i+1]) {
                System.out.println("Not-sorted");
                sorted = false;
                break;
            }
        }

        if (sorted) {
            System.out.println("Sorted");
        }


        for (int a = 1; a < array.length; a += 2) {
            array[a] = 0;
        }

        for (int j : array) {
            System.out.print(j + " ");
        }

    }
}
